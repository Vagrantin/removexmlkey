# README #

This tool is about clean up XML keys from a large number of files at once.

### What is this repository for? ###

* Quick summary

    With this tool, you can specify by modifying the code the XML key you want to clean up, then it will delete the same key recursively on all XML file inside the folder specified.
    
You have two scripts

1. removekey

    It will clean up the specified XML in the script ( Mainly for testing/troubleshooting purpose )

2. RemovekeyRecuresively.py

    It will clean up the specified key recursively in all files.
    
* Version

    0.1

### How do I get set up? ###

* Summary of set up

    If you have python on your machine you can run it with a python command 
    `python RemovekeyRecuresively.py "PATH\TO\FOLDER"`
    You can also compile it with pyinstaller, it will create a binary that can run without the need to install python.
    
* Configuration

    You need to specify the path of the folder you want to run it on. If nothing is provided it will default to the "XMls" folder at the same level of the python script.
    You need to specify the XML key that you want to delete on all your XML files.
    
* Dependencies

    Python 3.x if you run the script as is
    Or compile with pyinstaller it prior usage.

### Who do I talk to? ###

* Repo owner or admin

Matthieu Ducorps