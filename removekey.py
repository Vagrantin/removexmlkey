import xml.etree.ElementTree as et
import os

pathToXML = "Assets/Wrong Status July27/2464805_JJ9029589CJO_[TM]HAMSTER & GRETEL (1)__EJ CCJ.xml"

tree = et.parse(pathToXML)
root = tree.getroot()
for parentNode in root.findall(".//glossaryValue_Languages"):
    for nodeToRemove in parentNode.findall(".//GlossaryValue_Language[@languageId.l='110']"):
        print(et.tostring(nodeToRemove))
        parentNode.remove(nodeToRemove)

print(et.tostring(root))
tree.write(pathToXML, encoding="UTF8",xml_declaration=True)
