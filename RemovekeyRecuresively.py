import os
import xml.etree.ElementTree as et
import sys
from pathlib import Path

# specify the root directory
if len(sys.argv) < 2:
    root_dir = Path("XMLs")
    #print(len(sys.argv))
    print("You didn't provide any path, we will be using the default path: {}".format(root_dir))
else:
    #print(len(sys.argv))
    root_dir = Path(sys.argv[1])

if not root_dir.is_dir():
    print("path to directory not found please provide a valid path.")
    sys.exit(1)

# loop over the tuple returned by os.walk()
for path, subdirs, files in os.walk(root_dir):
    # loop over the files in each directory
    for file in files:
        # get the full path of the file
        file_path = os.path.join(path, file)
        #print(file_path)
        try:
            tree = et.parse(file_path)
            root = tree.getroot()
            for parentNode in root.findall(".//glossaryValue_Languages"):
                for nodeToRemove in parentNode.findall(".//GlossaryValue_Language[@languageId.l='110']"):
                    #print(et.tostring(nodeToRemove))
                    parentNode.remove(nodeToRemove)
            tree.write(file_path, encoding="UTF8",xml_declaration=True)
        except:
            print("parsing failed for {} ".format(file_path))
        
        #print(et.tostring(root))
